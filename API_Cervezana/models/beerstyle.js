const mongoose = require('mongoose');

const beerSchema = new mongoose.Schema(
{
    _id: {
        type: String,
        required: true
    },
    category: [
            {
                name: {
                    type: String
                },
                value: [
                    {
                        label: {
                            type: String
                        },
                        meaning: {
                            type: String
                        }
                    }
                ]
            }
        ]
});

module.exports = mongoose.model('beerstyle', beerSchema);