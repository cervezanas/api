const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: Number,
        required: true
    },
    phone: {
        type: Number,
        required: true
    },
    address: {
        type: Number
    },
    image: {
        type: String
    },
    date: {
        type: Date,
        required: true,
        default: Date.now
    },
    profile: [
        {
            _id: { type: String, required: true },
            name: { type: String, required: true }
        }
    ],
    userType: {
        type: String,
        required: true,
        default: "Users"
    }
})

module.exports = mongoose.model('users', userSchema)