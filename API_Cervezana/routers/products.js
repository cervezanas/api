const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const multer = require("multer");

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    }
    else {
        cb(new Error("message", "Only allowed image formats JPEG and PNG"), false);
    }
};

const upload = multer( { storage: storage, fileFilter: fileFilter } );

const Product = require("../models/product");
const ObjectID = require("mongodb").ObjectID;


// Getting all products
router.get("/", async (req, res) => {
    try {
       const products = await Product.find();
       let mes = { "error": false, "message": "Ok", "product":  products };
       res.status(200).json(mes);
    }
    catch (err) {
        res.status(500).json({ "error": true, "message": err.message });
    }
});

// Getting One product by ID
router.get("/:id", getProductbyID, (req, res) => {
    let mes = { "error": false, "message": "Ok", "product": res.product };
    res.status(200).json(mes);
});

// Creating One product
router.post("/", upload.single('productImage'), async (req, res) => {
    console.log(req.file);
    var objectId = new ObjectID();
    const product = new Product( {
        _id: objectId,
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        volume: req.body.volume,
        productImage: req.file.path
    });
    try {
        const newProduct = await product.save();
        let mes = { "error": false, "message": "The product was created successfully.", "product":  newProduct };
        res.status(201).json(mes);
    }
    catch (err) {
        res.status(400).json({ "error": true, "message": err.message });
    }
});

// Updating One product
router.patch('/:id', getProductbyID, async (req, res) => {
    if (req.body.name != null) {
        res.product.name = req.body.name
    }
    if (req.body.description != null) {
        res.product.description = req.body.description
    }
    if (req.body.price != null) {
        res.product.price = req.body.price
    }
    if (req.body.volume != null) {
        res.product.volume = req.body.volume
    }
    if (req.file.path != null) {
        res.product.productImage = req.file.path
    }
    try {
        const updatedProduct = await res.user.save()
        let mes = { "error": false, "message": "Update is Successful", "product":  updatedProduct}
        res.status(200).json(mes)
    }
    catch (err) {
        res.status(400).json({ "error": true, "message": err.message })
    }
})

// Deleting One product
router.delete('/:id', getProductbyID, async (req, res) => {
    try {
        await res.product.remove()
        res.status(200).json({ "error": false, "message": "Deleted product" })
    }
    catch (err) {
        res.status(500).json({ "error": true, "message": err.message })
    }
})

async function getProductbyID(req, res, next) {
    let product
    try {
        product = await Product.findById(req.params.id)
        if (product == null) {
            return res.status(404).json({ "error": true, "message": "Cannot find the product" })
        }
    }
    catch (err) {
        return res.status(500).json({ "error": true, "message": err.message })
    }
    res.product = product
    next()
}

module.exports = router