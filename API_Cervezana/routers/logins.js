const express = require('express')
const router = express.Router()
const User = require('../models/user')
const bodyParser = require('body-parser')

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({extended: true}))

// Getting One user by Phone
router.post('/', async (req, res) =>
{
    let user
    const phone = req.body.phone
    const password = req.body.password
    try
    {
        user = await User.findOne({ phone })
        if (user == null)
        {
            let mes = { "error": true, "message": "Cannot find user" }
            return res.status(200).json( mes )
        }
    }
    catch (err)
    {
        return res.status(500).json({ "error": true, "message": err.message })
    }

    res.user = user

    if (password != res.user.password)
    {
        let mes = { "error": true, "message": "Password is wrong" }
        return res.status(200).json( mes )
    }
    else
    {
        let mes = { "error": false, "message": "Loggin is Successful", "user":  res.user}
        //var merged_object = JSON.parse((JSON.stringify(mes) + JSON.stringify(res.user)).replace(/}{/g,","))
        
        res.status(200).json( mes )
    }
})

module.exports = router