const express = require('express')
const router = express.Router()
const Beer = require('../models/beerstyle')
const bodyParser = require('body-parser')

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({extended: true}))

// Getting All beer Styles
router.get('/', async (req, res) => {
    try {
        const beers = await Beer.find()
        if (beers == null) {
            return res.status(404).json({ "error": true, "message": "Cannot find beerstyle" })
        }
        else {
            let mes = { "error": false, "message": "Ok", "beer":  beers }
            res.status(200).json(mes)
        }
    }
    catch (err) {
        res.status(500).json({ "error": true, "message": err.message })
    }
})

// Getting One beer style by Category
router.get('/:name', getBeerbyName, (req, res) => {
    let mes = { "error": false, "message": "Ok", "beer":  res.beer }
    res.status(200).json(mes)
})

// Creating One BeerStyle
router.post('/', async (req, res) => {
    const beer = new Beer( {
        _id: "Prueba",
        Category: req.body.Category
    })
    try {
        const newBeer = await beer.save()
        let mes = { "error": false, "message": "Create Beer is Successful", "beer":  newBeer }
        res.status(201).json(mes)
    }
    catch (err) {
        let mes = { "error": true, "message": err.messag }
        res.status(400).json(mes)
    }
})

async function getBeerbyName(req, res, next) {
    let beer
    try {
        beer = await Beer.findOne({ name: req.params.name })
        if (beer == null) {
            return res.status(404).json({ "error": true, "message": "Cannot find category" })
        }
    }
    catch (err) {
        res.status(500).json({ "error": true, "message": err.message })
    }
    res.beer = beer
    next()
}

module.exports = router