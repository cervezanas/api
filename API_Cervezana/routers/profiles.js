const express = require('express')
const router = express.Router()
const Profile = require('../models/profile')
const ObjectID = require('mongodb').ObjectID

// Getting all profiles
router.get('/', async (req, res) => {
    try {
       const profiles = await Profile.find()
       let mes = { "error": false, "message": "Ok", "profile":  profiles}
       res.status(200).json(mes)
    }
    catch (err) {
        res.status(500).json({ "error": true, "message": err.message })
    }
})

// Getting One profile by Name
router.get('/:name', getProfilebyName, (req, res) => {
    res.send(res.profile)
})

// Creating One profile
router.post('/', async (req, res) => {
    var objectId = new ObjectID();
    const profile = new Profile( {
        _id: objectId,
        name: req.body.name
    })
    try {
        const newProfile = await profile.save()
        let mes = { "error": false, "message": "The profile was created successfully.", "profile":  newProfile}
        res.status(201).json(mes)
    }
    catch (err) {
        res.status(400).json({ "error": true, "message": err.message })
    }
})

// Updating One profile
router.patch('/:id', getProfilebyID, async (req, res) => {
    if (req.body.name != null) {
        res.profile.name = req.body.name
    }
    try {
        const updatedProfile = await res.profile.save()
        res.json(updatedProfile)
    }
    catch (err) {
        res.status(400).json({ "error": true, "message": err.message })
        
    }
})

async function getProfilebyID(req, res, next) {
    let profile
    try {
        profile = await Profile.findById(req.params.id)
        if (profile == null) {
            return res.status(400).json({ "error": true, "message": "Cannot find the profile" })
        }
    }
    catch (err) {
        res.status(500).json({ "error": true, "message": err.message })
    }
    res.profile = profile
    next()
}

async function getProfilebyName(req, res, next) {
    let profile
    try {
        profile = await Profile.findOne({ name: req.params.name })
        if (profile == null) {
            return res.status(400).json({ "error": true, "message": "Cannot find the profile" })
        }
    }
    catch (err) {
        res.status(500).json({ "error": true, "message": err.message })
    }
    res.profile = profile
    next()
}

module.exports = router