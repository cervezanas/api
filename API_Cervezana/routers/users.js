const express = require('express')
const router = express.Router()
const User = require('../models/user')
const bodyParser = require('body-parser')
const ObjectID = require('mongodb').ObjectID

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({extended: true}))

// Getting all users
router.get('/', async (req, res) => {
    try {
       const users = await User.find()
       let mes = { "error": false, "message": "Ok", "user":  users}
       res.status(200).json(mes)
    }
    catch (error) {
        res.status(500).json({ "error": true, "message": err.message })
    }
})

// Getting One user by ID
router.get('/:id', getUserbyID, (req, res) => {
    let mes = { "error": false, "message": "Ok", "user":  res.user }
    res.status(200).json(mes)
})

// Getting One user by Email
/** router.get('/:email', getUserbyEmail, (req, res) => {
    res.send(res.user)
})**/

// Creating One user
router.post('/', async (req, res) => {
    var objectId = new ObjectID();
    const user = new User( {
        _id: objectId,
        name: req.body.name,
        email: req.body.email,
        phone: req.body.phone,
        password: req.body.password,
        address: req.body.address,
        image: req.body.image,
        profile: req.body.profile
    })
    try {
        const newUser = await user.save()
        let mes = { "error": false, "message": "User was created successfully", "user":  newUser}
        res.status(201).json(mes)
    }
    catch (err) {
        res.status(400).json({ "error": true, "message": err.message })
    }
})

// Updating One user
router.patch('/:id', getUserbyID, async (req, res) => {
    if (req.body.name != null) {
        res.user.name = req.body.name
    }
    if (req.body.email != null) {
        res.user.email = req.body.email
    }
    if (req.body.phone != null) {
        res.user.phone = req.body.phone
    }
    if (req.body.password != null) {
        res.user.password = req.body.password
    }
    if (req.body.address != null) {
        res.user.address = req.body.address
    }
    if (req.body.image != null) {
        res.user.image = req.body.image
    }
    if (req.body.userType != null) {
        res.user.userType = req.body.userType
    }
    if (req.body.profile != null) {
        res.user.profile = req.body.profile
    }
    try {
        const updatedUser = await res.user.save()
        let mes = { "error": false, "message": 'Update is Successful', "user":  updatedUser}
        res.status(200).json(mes)
    }
    catch (err) {
        res.status(400).json({ "error": true, "message": err.message })
    }
})

// Deleting One user
router.delete('/:id', getUserbyID, async (req, res) => {
    try {
        await res.user.remove()
        res.status(200).json({ "error": false, "message": "Deleted User" })
    }
    catch (err) {
        res.status(500).json({ "error": true, "message": err.message })
    }
})

async function getUserbyID(req, res, next) {
    let user
    try {
        user = await User.findById(req.params.id)
        if (user == null) {
            return res.status(404).json({ "error": true, "message": "Cannot find user" })
        }
    }
    catch (err) {
        return res.status(500).json({ "error": true, "message": err.message })
    }

    res.user = user
    next()
}

async function getUserbyEmail(req, res, next) {
    let user
    try {
        user = await User.findOne({ email: req.params.email })
        if (user == null) {
            return res.status(404).json({ "error": true, "message": "Cannot find user" })
        }
    }
    catch (err) {
        return res.status(500).json({ "error": true, "message": err.message })
    }

    res.user = user
    next()
}

module.exports = router