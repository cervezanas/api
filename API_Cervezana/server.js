require("dotenv").config();

const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");

mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true });
const db = mongoose.connection;

db.on("error", (error) => console.error(error));
db.once("open", (error) => console.log("Connected to Database"));

app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const usersRouter = require('./routers/users');
const loginRouter = require('./routers/logins');
const profileRouter = require('./routers/profiles');
const beerStyleRouter = require('./routers/beerstyles');
const productRouter = require('./routers/products');

app.use('/users', usersRouter);
app.use('/logins', loginRouter);
app.use('/profiles', profileRouter);
app.use('/beerstyles', beerStyleRouter);
app.use('/products', productRouter);

app.listen(3030, () => console.log("Server Started"));